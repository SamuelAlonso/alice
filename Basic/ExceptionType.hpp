//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once
#include <Basic/Types.hpp>

enum class ExceptionType : u16
{
    None,
    MemoryPageInvalidSize,
    AppPathError,
    
};