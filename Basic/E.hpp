//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once
#include <Basic/Types.hpp>

constexpr f32 E32 = 2.718281746f;
constexpr f64 E64 = 2.718281828459045091;