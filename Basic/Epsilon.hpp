//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once
#include <Basic/Types.hpp>

constexpr f32 Epsilon32 = 1.401298464e-45f;
constexpr f64 Epsilon64 = 4.940656458412465442e-324;