//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once

#define NegativeInfinity32 -1.0f / 0.0f
#define NegativeInfinity64 -1.0 / 0.0
#define PositiveInfinity32 1.0f / 0.0f
#define PositiveInfinity64 1.0 / 0.0