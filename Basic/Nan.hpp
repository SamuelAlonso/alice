//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once

#define NaN32 0.0f / 0.0f
#define NaN64 0.0 / 0.0