//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once
#include <Basic/Types.hpp>

constexpr f32 PI32 = 3.141592741f;
constexpr f64 PI64 = 3.141592653589793116;