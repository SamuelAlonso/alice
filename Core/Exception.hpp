//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once
#include <Basic/Inline.hpp>
#include <Basic/ExceptionType.hpp>

namespace Alice
{
    class Exception
    {
    private:
        ExceptionType exceptiontype = ExceptionType::None;
    public:
        AliceInline constexpr Exception() noexcept{}

        AliceInline constexpr Exception(ExceptionType ExceptionToRaise) noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            exceptiontype = ExceptionToRaise;
            #endif
            #else
            exceptiontype = ExceptionToRaise;
            #endif
            #endif
        }

        AliceInline constexpr Exception(Exception& Other) noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            exceptiontype = Other.exceptiontype;
            #endif
            #else
            exceptiontype = Other.exceptiontype;
            #endif
            #endif
        }

        AliceInline constexpr Exception(const Exception& Other) noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            exceptiontype = Other.exceptiontype;
            #endif
            #else
            exceptiontype = Other.exceptiontype;
            #endif
            #endif
        }

        AliceInline constexpr Exception(Exception&& Other) noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            exceptiontype = Other.exceptiontype;
            Other.exceptiontype = ExceptionType::None;
            #endif
            #else
            exceptiontype = Other.exceptiontype;
            Other.exceptiontype = ExceptionType::None;
            #endif
            #endif
        }

        AliceInline constexpr void operator=(ExceptionType ExceptionToRaise) noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            exceptiontype = ExceptionToRaise;
            #endif
            #else
            exceptiontype = ExceptionToRaise;
            #endif
            #endif
        }

        AliceInline constexpr void operator=(Exception& Other) noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            exceptiontype = Other.exceptiontype;
            #endif
            #else
            exceptiontype = Other.exceptiontype;
            #endif
            #endif
        }

        AliceInline constexpr void operator=(const Exception& Other) noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            exceptiontype = Other.exceptiontype;
            #endif
            #else
            exceptiontype = Other.exceptiontype;
            #endif
            #endif
        }

        AliceInline constexpr void operator=(Exception&& Other) noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            exceptiontype = Other.exceptiontype;
            Other.exceptiontype = ExceptionType::None;
            #endif
            #else
            exceptiontype = Other.exceptiontype;
            Other.exceptiontype = ExceptionType::None;
            #endif
            #endif
        }

        AliceInline constexpr bool operator==(ExceptionType ExceptionToCheck) const noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            return ExceptionToCheck == exceptiontype;
            #else
            return false;
            #endif
            #else
            return ExceptionToCheck == exceptiontype;
            #endif
            #else
            return false;
            #endif
        }

        AliceInline constexpr bool operator==(Exception& Other) const noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            return exceptiontype == Other.exceptiontype;
            #else
            return false;
            #endif
            #else
            return exceptiontype == Other.exceptiontype;
            #endif
            #else
            return false;
            #endif
        }

        AliceInline constexpr bool operator==(const Exception& Other) const noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            return exceptiontype == Other.exceptiontype;
            #else
            return false;
            #endif
            #else
            return exceptiontype == Other.exceptiontype;
            #endif
            #else
            return false;
            #endif
        }

        AliceInline constexpr bool operator==(Exception&& Other) const noexcept
        {
            #ifndef AliceUnsafe
            #if defined(AliceExceptionsOnlyOnDebugMode)
            #if defined(AliceDebug)
            bool b = exceptiontype == Other.exceptiontype;
            if(b)
                Other.exceptiontype = ExceptionType::None;
            return b;
            #else
            return false;
            #endif
            #else
            bool b = exceptiontype == Other.exceptiontype;
            if(b)
                Other.exceptiontype = ExceptionType::None;
            return b;
            #endif
            #else
            return false;
            #endif
        }
    };

    Exception AliceException;
}