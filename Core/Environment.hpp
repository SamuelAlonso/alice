//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once
#include <Core/Exception.hpp>
#if defined(AliceWindows)
#include <Windows.h>
#else
#include <unistd.h>
#endif

namespace Alice
{
    namespace Environment
    {
        //Exceptions(excluding Windows): MemoryPageInvalidSize
        AliceInline s32 MemoryPageSize(Exception& DefaultException = AliceException) noexcept
        {
            #if defined(AliceWindows)
            SYSTEM_INFO si;
            GetSystemInfo(&si);
            return si.dwPageSize;
            #else
            s32 ret = sysconf(_SC_PAGE_SIZE);
            if(ret == -1)
            {
                DefaultException = ExceptionType::MemoryPageInvalidSize;
                return 0;
            }
            else
                return ret;
            #endif
        }
    }
}