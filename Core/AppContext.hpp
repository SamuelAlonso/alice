//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once
#include <Core/Exception.hpp>
#include <Basic/Database.hpp>
#if defined(AliceWindows)
#include <Windows.h>
#else
#include <unistd.h>
#endif

namespace Alice
{
    namespace AppContext
    {
        AliceInline s32 GetApplicationID() noexcept
        {
            #if defined(AliceWindows)
            return GetCurrentProcessId();
            #else
            return getpid();
            #endif
        }

        //Exceptions: AppPathError
        AliceInline void GetExecutableDirectory(Exception& DefaultException = AliceException) noexcept
        {
            #if defined(AliceWindows)
            char path[MAX_PATH];
            if(GetModuleFileNameA(0, path, MAX_PATH))
                DefaultException = ExceptionType::AppPathError;
            else
                ExecutablePath = path;
            #else
            char path[PATH_MAX];
            if(!getcwd(path, PATH_MAX))
                DefaultException = ExceptionType::AppPathError;
            else
                ExecutablePath = path;
            #endif
        }

        //Exceptions: AppPathError
        AliceInline void GetBaseDirectory(bool IncludeLastSeparator = false, Exception& DefaultException = AliceException) noexcept
        {
            #if defined(AliceWindows)
            char BaseDir[MAX_PATH];
            if(GetModuleFileNameA(0, BaseDir, MAX_PATH))
                DefaultException = ExceptionType::AppPathError;
            else
            {
                bool Stop = false;
                s32 Index = MAX_PATH;
                char path[MAX_PATH];
                while(Index > 0)
                {
                    --Index;
                    if(!Stop)
                    {
                        if(BaseDir[Index] == '\\')
                        {
                            Stop = true;
                            if(!IncludeLastSeparator)
                                path[Index] = '\0'; 
                        }
                        else
                            path[Index] = '\0';
                    }
                    else
                        path[Index] = BaseDir[Index];
                }
                BaseDirectoryPath = path;
            }
            #else
            char BaseDir[PATH_MAX];
            if(!getcwd(BaseDir, PATH_MAX))
                DefaultException = ExceptionType::AppPathError;
            else
            {
                bool Stop = false;
                s32 Index = MAX_PATH;
                char path[MAX_PATH];
                while(Index > 0)
                {
                    --Index;
                    if(!Stop)
                    {
                        if(BaseDir[Index] == '\\')
                        {
                            Stop = true;
                            if(!IncludeLastSeparator)
                                path[Index] = '\0'; 
                        }
                        else
                            path[Index] = '\0';
                    }
                    else
                        path[Index] = BaseDir[Index];
                }
                BaseDirectoryPath = path;
            }
            #endif
        }
    }
}