//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once
#include <Basic/Inline.hpp>
#include <Concepts/RemoveReferences.hpp>

namespace Alice
{
    template<class T> AliceInline constexpr T&& Forward(Concepts::RemoveReferences<T>& Value) noexcept
    {
        return static_cast<T&&>(Value);
    }

    template<class T> AliceInline constexpr T&& Forward(Concepts::RemoveReferences<T>&& Value) noexcept
    {
        return static_cast<T&&>(Value);
    }
}