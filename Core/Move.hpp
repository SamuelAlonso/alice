//Copyright 2020 Iris Technologies, All Rights Reserved
#pragma once
#include <Basic/Inline.hpp>
#include <Concepts/RemoveReferences.hpp>

namespace Alice
{
    template<class T> AliceInline constexpr Concepts::RemoveReferences<T>&& Move(T&& value) noexcept
    {
        return value;
    }
}